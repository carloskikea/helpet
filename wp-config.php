<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'db:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GBadsd8_jE6ztJH@sSUl[~)gZ4FXnfquB/5z|&PIjvvMHUr|bv?pS]%&_W%*8e s');
define('SECURE_AUTH_KEY',  '5!7k^9F.TjOf;yb>Vt0^P-~Av71v0{jPs6Km)d<-[B2l t?q^JU#P*f&Jz[05=cd');
define('LOGGED_IN_KEY',    'f|HN[=ggER4oNSY5gBlp${a)2c8t^m6A:K*&f+LXb[jrnV>.)fcnlxG-OQk7)Ewx');
define('NONCE_KEY',        'h<ozYSmg|{R;L6etRt+W.IBih4J&Fa]pkN}p7^f=T8~(;6_8t( _T+f-zoFU_.#J');
define('AUTH_SALT',        'h=24D,)eaz|z(6Y_8y<HT,V81#1ZF|LYmn/aLL(4{T}?)t(&%1!1=h@Hm_hJYe6*');
define('SECURE_AUTH_SALT', 'hGHi<tZ1Zs:2pLTe3jMM1$y~v)nKP6=aQ[Yt43^>:`LEQevOD*mw_2>v!wLp3cL@');
define('LOGGED_IN_SALT',   'UlhYwAg#LP:b_;2Xi%5lefasw)CeNxRv+&*p#XL^z-WuLeMH/O Exe~mIf$Dg?*g');
define('NONCE_SALT',       'o%=[F9gCY5A&cNaX_,~K?ptYGiN5l^qwF)eF1S8EKdX,%n9O#bC0:k;f}fB_hO!d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
