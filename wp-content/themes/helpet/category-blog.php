<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage HelPet
 * @since 1.0
 * @version 1.0
 */

get_header('blog'); ?>

<section id="blog" class="categories">
	<?php $idObj = get_category_by_slug('blog'); ?>
    <div class="container">
        <div class="row">

            <?php                 
                $categoryId = $idObj->term_id;

                query_posts("cat=$categoryId");
                if(have_posts()) : while(have_posts()) : the_post(); ?>

                    <?php
                        $title = get_the_title();
                        $content = get_the_excerpt();
                    ?>

                    <div class="col-md-4 col-sm-6 portfolio-item">
                        <a href="<?php the_permalink() ?>" class="portfolio-link" data-toggle="modal">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content">
                                    <i class="fa fa-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?php the_post_thumbnail_url() ?>" class="img-responsive" alt="">
                        </a>
                        <div class="blog-caption">
                            <h4 class="post-title"><?= $title ?></h4> 
                            <p class="post-content"><?= $content ?></p>
                            <a href="<?php the_permalink() ?>" class="btn btn-readmore">LEER MAS</a>
                        </div>
                    </div>    
                <?php endwhile; endif;            
            ?>

        </div>
    </div>
</section>

<?php get_footer();