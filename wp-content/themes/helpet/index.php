<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?> 

<?php  get_header(); ?> 
  
  <?php get_template_part( 'template-parts/content/services/content', 'services' ); ?>

  <?php get_template_part( 'template-parts/content/histories/content', 'histories' ); ?>

  <?php get_template_part( 'template-parts/content/about/content', 'about' ); ?>

  <?php get_template_part( 'template-parts/content/blog/content', 'blog' ); ?>

  <?php get_template_part( 'template-parts/content/team/content', 'team' ); ?>

  <!-- <?php get_template_part( 'template-parts/content/partners/content', 'partners' ); ?> -->

<?php get_footer();
