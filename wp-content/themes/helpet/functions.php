<?php

function theme_styles() { 
   wp_enqueue_style( 'bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
   wp_enqueue_style( 'montserrat_font', 'https://fonts.googleapis.com/css?family=Montserrat:400,700' );
   wp_enqueue_style( 'kaushan_font', 'https://fonts.googleapis.com/css?family=Kaushan+Script' );
   wp_enqueue_style( 'droid_font', 'https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' );
   wp_enqueue_style( 'roboto_font', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' );
   wp_enqueue_style( 'font_awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
   wp_enqueue_style( 'lato', 'http://fonts.googleapis.com/css?family=Lato:400,700' );
   wp_enqueue_style( 'Comfortaa', 'https://fonts.googleapis.com/css?family=Comfortaa:300,400' );
   wp_enqueue_style( 'main_css', get_stylesheet_directory_uri() . '/style.css' );
} 
add_action( 'wp_enqueue_scripts', 'theme_styles');

function theme_js() {
  wp_register_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', null, null, true );
  wp_enqueue_script('jQuery');
  wp_register_script( 'bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', null, null, true);
  wp_enqueue_script('bootstrap_js');
  wp_register_script( 'helpet_js', '/wp-content/themes/helpet/assets/js/helpet.js', null, null, true);
  wp_enqueue_script('helpet_js');
} 
add_action( 'wp_enqueue_scripts', 'theme_js');

function custom_callback($comment, $args, $depth) {

    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }?>
    <<?php echo $tag; ?> class="comment even thread-even depth-1 comment"  id="comment-<?php comment_ID() ?>"><?php 
    if ( 'div' != $args['style'] ) { ?>
        <?php
    } ?>

        <div class="comment-avatar"><?php 
            if ( $args['avatar_size'] != 0 ) {
                echo get_avatar( $comment, $args['avatar_size'] ); 
            } ?>
        </div>

        <div class="comment-body">
          <div class="comment-title">
            
            <div class="comment-author">
              <h2><?= get_comment_author_link() ?></h2>
            </div>
            <div class="comment-date">
                <b><?php
                    /* translators: 1: date, 2: time */
                    printf( 
                        __('%1$s at %2$s'), 
                        get_comment_date(),  
                        get_comment_time() 
                    ); ?>
                </b><?php 
                edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
            </div>

          </div>

          <div class="comment-content">
            <?php comment_text(); ?>
          </div>
        </div>
        <?php 
    if ( 'div' != $args['style'] ) : ?>
        <?php 
    endif;
}

function themename_custom_header_setup() {
    $defaults = array(
        // Default Header Image to display
        'default-image'         => get_parent_theme_file_uri( '/assets/images/blogs.png' ),
        // Display the header text along with the image
        'header-text'           => false,
        // Header text color default
        'default-text-color'        => '000',
        // Header image width (in pixels)
        'width'             => 1000,
        // Header image height (in pixels)
        'height'            => 150,
        // Header image random rotation default
        'random-default'        => false,
        // Enable upload of image file in admin 
        'uploads'       => false,
        // function to be called in theme head section
        'wp-head-callback'      => 'wphead_cb',
        //  function to be called in preview page head section
        'admin-head-callback'       => 'adminhead_cb',
        // function to produce preview markup in the admin screen
        'admin-preview-callback'    => 'adminpreview_cb',
        );
}
add_action( 'after_setup_theme', 'themename_custom_header_setup' );

function crunchify_disable_comment_url($fields) { 
    unset($fields['url']);
    unset($fields['email']);
    return $fields;
}
add_filter('comment_form_default_fields','crunchify_disable_comment_url');

?>
