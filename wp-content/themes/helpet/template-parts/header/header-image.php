<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>
<div class="container">
    <div class="intro-text">
        <div class="intro-lead-in"><span class="titlte-first-home">Hel</span><span class="titlte-second-home">Pet</span></div>
        <div class="intro-heading"><span class="subtitle-home">¡Todo por ellos!</span></div>
        <a href="#services" class="page-scroll btn btn-xl">ver más</a>
    </div>
</div>