<?php
/**
 * Displays header site titles
 *
 * @package WordPress
 * @subpackage helepet
 * @since 1.0
 * @version 1.0
 */
?>

<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class=" navbar-brand page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?= get_parent_theme_file_uri( '/assets/images/logo.png' ) ?>" width="100px" height="100px">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <?php if (is_home()): ?>
                      <a class="page-scroll" href="#services">Servicios</a>
                    <?php else:; ?>
                      <a class="page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#services">Servicios</a>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (is_home()): ?>
                        <a class="page-scroll" href="#portfolio">Historias</a>
                    <?php else:; ?>
                        <a class="page-scroll" href="<?= get_category_link( get_category_by_slug('historias')->term_id ) ?>">Historias</a>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (is_home()): ?>
                        <a class="page-scroll" href="#about">Nosotros</a>
                    <?php else:; ?>
                        <a class="page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#about">Nosotros</a>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (is_home()): ?>
                        <a class="page-scroll" href="#blog">Blog</a>
                    <?php else:; ?>
                        <a class="page-scroll" href="<?= get_category_link( get_category_by_slug('blog')->term_id ) ?>">Blog</a>
                    <?php endif; ?>
                </li>
                <li>
                    <?php if (is_home()): ?>
                        <a class="page-scroll" href="#team">Equipo</a>
                    <?php else:; ?>
                        <a class="page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#team">Equipo</a>
                    <?php endif; ?>
                </li>                
            </ul>
        </div>
    </div>
</nav>