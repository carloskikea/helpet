<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>

<div class="site-branding" style="margin-bottom: 0px;">
	<div class="container">
		<div class="intro-text-category">	            
    		<div class="intro-lead-in"><span class="subtitle-home">Blog's</span></div>
		</div>
	</div>
</div>

