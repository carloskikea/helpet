<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage HelPet
 * @since 1.0
 * @version 1.2
 */

?>

<article class="post type-post status-publish format-gallery has-post-thumbnail hentry category-post-formats tag-format tag-gallery post_format-post-format-gallery post-has-thumbnail" id="post-<?php the_ID(); ?>" 	>

	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>	

	<?php
		if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
			echo '<div class="single-featured-image-header">';
			echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
			echo '</div><!-- .single-featured-image-header -->';
		endif;
	?>

	<div class="post-body">

		<?php if (in_category('blog')): ?>
			<header class="entry-header">
				<?php
					#<!-- Title -->
					if ( is_single() ) {
						the_title( '<h1 class="blog-title">', '</h1>' );
					} elseif ( is_front_page() && is_home() ) {
						the_title( '<h3 class="blog-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
					} else {
						the_title( '<h2 class="blog-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					}
				?>
			</header>
		<?php endif; ?>

		<?php if (in_category('blog')): ?>
			<div>
				<?php 
	                $author = get_post_custom_values( 'author' )[0];
					if ( 'post' === get_post_type() ) {
						echo '<p class="entry-meta">';
							echo '<spam class="hh">Escrito por: </spam>';
							if ( is_single() ) {
								echo $author; 
								echo ", ";
								echo twentyseventeen_time_link();
							} else {
								echo twentyseventeen_time_link();
								twentyseventeen_edit_link();
							};
						echo '</p><!-- .entry-meta -->';
					};
				?>
			</div>
		<?php endif; ?>

		<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
				</a>
			</div><!-- .post-thumbnail -->
		<?php endif; ?>

		<div class="post-content">
			<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
				get_the_title()
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
			?>
		</div><!-- .entry-content -->

		<?php if (in_category('historias')): ?>
			<div>
				<?php 
	                $author = get_post_custom_values( 'author' )[0];
					if ( 'post' === get_post_type() ) {
						echo '<p class="entry-meta">';
							echo '<spam class="hh">Compartido por: </spam>';
							if ( is_single() ) {
								echo $author; 
								echo ", ";
								echo twentyseventeen_time_link();
							} else {
								echo twentyseventeen_time_link();
								twentyseventeen_edit_link();
							};
						echo '</p><!-- .entry-meta -->';
					};
				?>
			</div>
		<?php endif; ?>
	</div>

</article>


