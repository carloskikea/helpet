<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>

<section id="team" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Equipo</h2>
                <h3 class="section-subheading text-muted">Quienes lideramos esta labor</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?= get_parent_theme_file_uri('/assets/images/leidy.png') ?>" class="img-circle" alt="">
                    <h4>Leidy Correa</h4>
                    <p class="text-muted">Líder de Mercado</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="https://www.instagram.com/leidycorream" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/leidycorream" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/in/leidy-correa-49698079" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?= get_parent_theme_file_uri('/assets/images/mateo.png') ?>"  class="img-circle" alt="">
                    <h4>Mateo Garcia</h4>
                    <p class="text-muted">Líder de Diseño</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="https://www.instagram.com/mateo.g7" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/mateo2907" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/in/mateog07" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img src="<?= get_parent_theme_file_uri('/assets/images/carlos.png') ?>" class="img-circle" alt="">
                    <h4>Carlos Agudelo</h4>
                    <p class="text-muted">Líder Desarrollo</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="https://twitter.com/carloskike92" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/carloskikea" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/in/agudelocarlos" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <p class="large text-muted">Aportando nuestro grano de arena en busca de la solución del flagelo animal</p>
            </div>
        </div>
    </div>
</section>
