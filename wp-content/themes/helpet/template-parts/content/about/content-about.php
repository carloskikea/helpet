<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>

<section id="about" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">nosotros</h2>
                <h3 class="section-subheading text-muted">Conoce más acerca de HelPet y por qué realizamos esta labor</h3>
            </div>
        </div>
        <div class="row">                
            <div class="col-lg-12 text-center">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Y1RpMLDrMyA?controls=0">
                    </iframe>
                </div>
            </div>                
        </div>
    </div>
</section>