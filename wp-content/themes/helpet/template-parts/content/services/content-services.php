<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>

<section id="services" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Servicios</h2>
                <h3 class="section-subheading text-muted">Agrupamos en cuatro ejes fundamentales nuestro actuar</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-3">
                <span class="fa-stack fa-3x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-paw fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Fauna callejera</h4>
                <p class="text-muted">Dignificamos la vida de las mascotas en situación de calle a través de una red de ayuda cooperativa</p>
            </div>
            <div class="col-md-3">
                <span class="fa-stack fa-3x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-home fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Población Adoptable</h4>
                <p class="text-muted">Actuamos de manera responsable y directa sobre las fundaciones y hogares que necesitan nuestro apoyo</p>
            </div>
            <div class="col-md-3">
                <span class="fa-stack fa-3x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-bug fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Reportes de Maltrato</h4>
                <p class="text-muted">Contamos con una sistema de alerta en tiempo real de aquellas mascotas que necesitan nuestra ayuda</p>
            </div>                
            <div class="col-md-3">
                <span class="fa-stack fa-3x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-area-chart  fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Control Poblacional</h4>
                <p class="text-muted">Enfocamos esfuerzos que mitigan de manera considerable el crecimiento de mascotas desprotegidas</p>
            </div>
        </div>
    </div>
</section>