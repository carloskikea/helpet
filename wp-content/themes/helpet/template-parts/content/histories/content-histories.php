<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>

<section id="portfolio">
    <?php $idObj = get_category_by_slug('historias');  ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Historias</h2>
                <h3 class="section-subheading text-muted">Presentamos historias de nuestra comunidad, seguro te sentirás identificado</h3>
            </div>
        </div>
        <div class="row">
            <?php 
                $categoryId = $idObj->term_id;
                $posts = 1;
                $args = array(
                    'orderby' => 'rand',
                    'cat'    => $categoryId
                );
                query_posts($args);
                if(have_posts()) : while($posts <= 3) : the_post(); ?>

                    <?php
                      $title = get_the_title();
                      $city = get_post_custom_values( 'city' )[0];
                      $country = get_post_custom_values( 'country' )[0];
                      $posts++;
                    ?> 

                    <div class="col-md-4 col-sm-6 portfolio-item">
                        <a href="<?php the_permalink() ?>" class="portfolio-link" data-toggle="modal">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content">
                                    <i class="fa fa-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?php the_post_thumbnail_url() ?>" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4 class="history-title"><?= $title ?></h4>
                            <p class="text-muted"><?= $city ?>, <?= $country ?></p>
                        </div>
                    </div>    
                    
                <?php endwhile; endif;            
            ?>
        </div>
    </div>
    <div class="col-lg-12 text-center">
        <a href="<?= get_category_link($idObj) ?>" class="page-scroll btn btn-xl">VER MÁS</a>
    </div>   
</section>