<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Helpet
 * @since 1.0
 * @version 1.0
 */

?>

<aside class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <a href="#">
                    <img src="img/logos/food1.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#">
                    <img src="img/logos/veterinaria1.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#">
                    <img src="img/logos/food2.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
        </div>
    </div>
</aside>