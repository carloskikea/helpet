<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

	</div><!-- #content -->

	<footer >
		<div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright"><b>Encuentranos en</b></span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://www.facebook.com/helpet.com.co" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.instagram.com/helpet.com.co/" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li><a href="https://www.youtube.com/channel/UCyoUIaGAYlmsClo1FfzN88A" target="_blank"><i class="fa fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><b>Hecho con mucho</b> <i class="fa fa-heart heart"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->

	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
