<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer >
			<div class="container">
	            <div class="row">
	                <div class="col-md-4">
	                    <span class="copyright">Encuentranos en</span>
	                </div>
	                <div class="col-md-4">
	                    <ul class="list-inline social-buttons">
	                        <li><a href="#"><i class="fa fa-twitter"></i></a>
	                        </li>
	                        <li><a href="#"><i class="fa fa-facebook"></i></a>
	                        </li>
	                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
	                        </li>
	                    </ul>
	                </div>
	                <div class="col-md-4">
	                    <ul class="list-inline quicklinks">
	                        <li><a href="#">Todos los derechos reservados</a>
	                        </li>
	                    </ul>
	                </div>
	            </div>
	        </div>
		</footer><!-- #colophon -->

	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
